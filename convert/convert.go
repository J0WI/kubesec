package convert

import (
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"bytes"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const (
	scannerID   = "kubesec"
	scannerName = "Kubesec"
	pathOutput  = "gl-sast-report.json"
)

// Decodes one native report from the tool (kubesec)
// and returns a KubesecFindings
func decodeReport(reader io.Reader) (*KubesecFindings, error) {
	var fileReport KubesecFindings

	dec := json.NewDecoder(reader)
	// read open bracket
	_, err := dec.Token()
	if err != nil {
		return nil, err
	}

	for dec.More() {
		err := dec.Decode(&fileReport)
		if err != nil {
			return nil, err
		}
	}

	return &fileReport, nil
}

// Convert reads multiple native reports from the tool (kubesec) and transforms
// them into issues (vulnerabilities), as defined in the common module
func Convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	dec := json.NewDecoder(reader)
	// read open bracket
	_, err := dec.Token()
	if err != nil {
		return nil, err
	}

	var fileReports []KubesecReport
	for dec.More() {
		var kubesecOutput KubesecOutput
		err := dec.Decode(&kubesecOutput)
		if err != nil {
			return nil, err
		}

		findingsReader := bytes.NewReader(kubesecOutput.Findings)

		findings, err := decodeReport(findingsReader)
		if err != nil {
			return nil, err
		}

		kubesecReport := KubesecReport{kubesecOutput.Filepath, *findings}
		fileReports = append(fileReports, kubesecReport)
	}

	scanner := issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	// Create issues from kubesec report
	issues := make([]issue.Issue, 0)

	for _, report := range fileReports {
		for _, vulnerability := range report.vulnerabilities() {
			selectors := []string{vulnerability.Object, vulnerability.Selector}
			objectSelector := strings.Join(selectors, " ")

			newIssue := issue.Issue{
				Category:    issue.CategorySast,
				Scanner:     scanner,
				Name:        vulnerability.Selector,
				Message:     objectSelector,
				Description: vulnerability.Reason,
				CompareKey:  vulnerability.compareKey(),
				Severity:    vulnerability.Severity,
				Confidence:  vulnerability.Confidence,
				Location: issue.Location{
					File:  vulnerability.Filepath,
					Class: vulnerability.Object,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "kubesec_rule_id",
						Name:  fmt.Sprintf("Kubesec Rule ID %s", vulnerability.Selector),
						Value: vulnerability.Reason,
					},
				},
			}

			issues = append(issues, newIssue)
		}
	}

	report := issue.NewReport()
	report.Vulnerabilities = issues
	return &report, nil
}
